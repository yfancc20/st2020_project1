
let sorting = (array) => {
    return array.sort();
}

let compare = (a, b) => {
    return a['PM2.5'] - b['PM2.5'];
}

let average = (nums) => {
    return Number((nums.reduce((a, b) => a + b, 0) / nums.length).toFixed(2));
}


module.exports = {
    sorting,
    compare,
    average
}