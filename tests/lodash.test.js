const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect;
const assert = chai.assert;


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})


test('Should compact an array with all falsey values removed', () => {
    const array = [0, 1, false, 2, '', 3, null, undefined, 4, 5, NaN];
    let compactArr = _.compact(array);
    expect(compactArr).to.eql([1, 2, 3, 4, 5]);
})


test('Should concat the elements to an array', () => {
    const arrayA = [1, 2];
    const arrayB = ['James', 'Hello'];
    let concatArr = _.concat(arrayA, true, arrayB);
    expect(concatArr).to.eql([1, 2, true, 'James', 'Hello']);
})


test('Should choose values not included in the given array', () => {
    const array = [1, 2, 3, '4', 5];
    let differenceArr = _.difference(array, [2, 3]);
    expect(differenceArr).to.eql([1, '4', 5]);
})


test('Should drop n elements from the array', () => {
    const array = [
        {'Name': 'Alice', 'Age': 22},
        {'Name': 'Bob', 'Age': 21},
        {'Name': 'Chelsea', 'Age': 23},
        {'Name': 'David', 'Age': 15},
        {'Name': 'Eva', 'Age': 25},
    ];
    let dropArr = _.drop(array, 4);
    expect(dropArr).to.eql([{'Name': 'Eva', 'Age': 25}]);
})


test('Should fill the array with given values', () => {
    let array = [1, 2, 3, '4', 5];
    let fillArr = _.fill(array, '%', 1, 3)
    expect(fillArr).to.eql([1, '%', '%', '4', 5]);
})


test('Should flatten array to a single level', () => {
    const array = [1, 2, [3, 4], [5, 6, [7]]];
    let flattenArr = _.flatten(array);
    expect(flattenArr).to.eql([1, 2, 3, 4, 5, 6, [7]]);
})


test('Should flatten deep the array to a single level', () => {
    const array = [1, [2, 3], [[[[4, 5]]]]];
    let flattenArr = _.flattenDeep(array);
    expect(flattenArr).to.eql([1, 2, 3, 4, 5]);
})


test('Should return the first element', () => {
    let array = ['Hello World!', 'Two sum', '3sum'];
    expect(_.head(array)).to.eql('Hello World!');

    array = [];
    assert.isUndefined(_.head(array));
})


test('Should get all but the last elements of the array', () => {
    const array = [1, 2, 3, 'should disappear'];
    expect(_.initial(array)).to.eql([1, 2, 3]);
})


test('Should get unique elements that in both arrays', () => {
    const arrayA = [1, '2', '3', '4', 'Match'];
    const arrayB = [2, '3', 4, 'Match'];
    let intersectionArr = _.intersection(arrayA, arrayB);
    expect(intersectionArr).to.eql(['3', 'Match']);
})